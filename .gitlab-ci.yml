stages:
  - build
  - deploy
  
variables:
  ROS_CI_DESKTOP: "`lsb_release -cs`"
  GIT_CLONE_PATH: $CI_BUILDS_DIR/autoware.ai/src/$CI_PROJECT_NAME
  # CI_PROJECT_DIR gets the form GIT_CLONE_PATH
  ROSINSTALL_FILE: $CI_PROJECT_DIR/dependencies.rosinstall
  CATKIN_OPTIONS: $CI_PROJECT_DIR/catkin.options
  GIT_SUBMODULE_STRATEGY: recursive

.build: &build_common
  before_script:
    - apt-get update && apt-get upgrade -y
    - apt-get install -y libarmadillo-dev libglew-dev libssh2-1-dev python-flask python-requests wget
    - apt-get install -y python3-pip python3-setuptools
    # Update setuptools from PyPI because the version Ubuntu ships with is too old
    - pip3 install -U setuptools
    - rosdep update
    - wget https://raw.githubusercontent.com/autowarefoundation/autoware/master/docker/generic/dependencies
    - sed -i "s/\$ROS_DISTRO/$ROS_DISTRO/g" "dependencies" && cat dependencies | xargs apt-get install -y    

build_kinetic:
  stage: build
  image: ros:kinetic
  variables:
    ROS_DISTRO: kinetic
  <<: *build_common
  script:
  # colcon as a Debian package is only available in Kinetic and up
    - apt-get install -y python3-colcon-common-extensions
  # Install lcov from source as binary installation errors when appending files
    - git clone https://github.com/linux-test-project/lcov.git
    - cd lcov
    - git checkout v1.13
    - make install 
    - cd ..
    - rm -r lcov
    - cd $CI_PROJECT_DIR/..
  # Clone messages
    - git clone https://gitlab.com/autowarefoundation/autoware.ai/messages.git
  # We first build the entire workspace normally
    - colcon build --cmake-args -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage" -DCMAKE_C_FLAGS="${CMAKE_C_FLAGS} -fprofile-arcs -ftest-coverage" -DCMAKE_BUILD_TYPE=Debug
  # And then build the tests target. catkin (ROS1) packages add their tests to the tests target
  # which is not the standard target for CMake projects. We need to trigger the tests target so that
  # tests are built and any fixtures are set up.
    - colcon build --cmake-target tests --cmake-args -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage" -DCMAKE_C_FLAGS="${CMAKE_C_FLAGS} -fprofile-arcs -ftest-coverage" -DCMAKE_BUILD_TYPE=Debug
    - lcov --initial --directory build --capture --output-file lcov.base
    - colcon test
    - colcon test-result
    - lcov --directory build --capture --output-file lcov.test
    - lcov -a lcov.base -a lcov.test -o lcov.total
    - lcov -r lcov.total '*/tests/*' '*/test/*' '*/build/*' '*/devel/*' '*/install/*' '*/log/*' '/usr/*' '/opt/*' '/tmp/*' '*/CMakeCCompilerId.c' '*/CMakeCXXCompilerId.cpp' -o lcov.total.filtered
    # BRANCH_NAME: gets branch name from CI_COMMIT_REF_NAME variable substituting / with _ (feature/test_this_lib becomes feature_test_this_lib)
    # CI_COMMIT_REF_NAME: (from https://docs.gitlab.com/ee/ci/variables/) The branch or tag name for which project is built
    - BRANCH_NAME="$(echo $CI_COMMIT_REF_NAME | sed 's/\//_/g')"
    - COVERAGE_FOLDER_NAME="coverage_$BRANCH_NAME"
    - genhtml -p "$PWD" --legend --demangle-cpp lcov.total.filtered -o $COVERAGE_FOLDER_NAME
    - tar -czvf coverage.tar.gz $COVERAGE_FOLDER_NAME
    - mv coverage.tar.gz $CI_PROJECT_DIR/coverage.tar.gz
  artifacts:
    paths:
      - $CI_PROJECT_DIR/coverage.tar.gz
    expire_in: 48 hrs 
  coverage: /\s*lines.*:\s(\d+\.\d+\%\s\(\d+\sof\s\d+.*\))/
  only:
    - merge_requests
    - master
    
build_cross:
  stage: build
  image: docker
  services:
    - docker:dind
  variables:
    ROS_DISTRO: kinetic
    AUTOWARE_DOCKER_DATE: 20190516
    AUTOWARE_HOME: $CI_PROJECT_DIR/..
    AUTOWARE_TARGET_ARCH: aarch64
    AUTOWARE_TARGET_PLATFORM: generic-aarch64
    AUTOWARE_BUILD_PATH: $CI_PROJECT_DIR/build-${AUTOWARE_TARGET_PLATFORM}
    AUTOWARE_INSTALL_PATH: $CI_PROJECT_DIR/install-${AUTOWARE_TARGET_PLATFORM}
    AUTOWARE_TOOLCHAIN_FILE_PATH: $CI_PROJECT_DIR/cross_toolchain.cmake
    AUTOWARE_SYSROOT: /sysroot/${AUTOWARE_TARGET_PLATFORM}
  script:
    # - ${AUTOWARE_HOME}/docker/crossbuild/build_cross_image.sh
    - wget https://raw.githubusercontent.com/autowarefoundation/autoware/master/ros/cross_toolchain.cmake  
    - 'docker run
          -e AUTOWARE_SYSROOT=${AUTOWARE_SYSROOT}
          --rm
          -v ${AUTOWARE_HOME}:${AUTOWARE_HOME}
          -w ${AUTOWARE_HOME}
          autoware/build:generic-aarch64-kinetic-20190516
          bash
            -c "
                apt-get update &&
                apt-get install -y git &&
                cd $CI_PROJECT_DIR/.. &&
                git clone https://gitlab.com/autowarefoundation/autoware.ai/messages.git &&
                source ${AUTOWARE_SYSROOT}/opt/ros/kinetic/setup.bash &&
                colcon build
                    --merge-install
                    --build-base ${AUTOWARE_BUILD_PATH}
                    --install-base ${AUTOWARE_INSTALL_PATH}
                    --cmake-args
                    -DCMAKE_TOOLCHAIN_FILE=${AUTOWARE_TOOLCHAIN_FILE_PATH}
                    -DCMAKE_SYSTEM_PROCESSOR=${AUTOWARE_TARGET_ARCH}
                    -DCMAKE_PREFIX_PATH=\"${AUTOWARE_SYSROOT}/opt/ros/kinetic;${AUTOWARE_INSTALL_PATH}\"
                    -DCMAKE_FIND_ROOT_PATH=${AUTOWARE_INSTALL_PATH}
                  "
                  '
  only:
    - merge_requests
    - master

pages:
  stage: deploy
  image: alpine
  dependencies:
    - build_kinetic
  script:
    - BRANCH_NAME="$(echo $CI_COMMIT_REF_NAME | sed 's/\//_/g')"
    - COVERAGE_FOLDER_NAME="coverage_$BRANCH_NAME"
    - tar -xzvf coverage.tar.gz
    - mv $COVERAGE_FOLDER_NAME public
  artifacts:
    paths:
      - public
  only:
    - master
